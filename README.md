**DS_Store Parser**

Extract filenames from .DS_Store file.

Dependencies:
Python3
ds_store (https://github.com/al45tair/ds_store python3 -m pip install --user ds_store)

---
**Example Usage**

```
python3 ds_store_parser.py -i ~/Downloads/DS_Store
index.php
hidden_backups.zip
favicon.png
```
