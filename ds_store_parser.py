#!/usr/bin/env python3
"""
.       .1111...          | Title: DS_Store Parser
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | Extract filenames from .DS_Store file.
                   ..     |
GrimHacker        ..      | Requires: ds_store 
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |
----------------------------------------------------------------------------
Created on 2019-04-29
@author: GrimHacker
DS_Store Parser - extract filenames from .DS_Store file.
    Copyright (C) 2019  Oliver Morton

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__version__ = "$Revision: 2.0 $"
# $Source$


import argparse
import logging
import json

try:
    from ds_store import DSStore
    import ds_store
except:
    print("Cannot import ds_store. run python3 -m pip --user install ds_store")
    exit()

def parse_dstore_data(filename):
    entries = {
        "directories": set(),
        "files": set()
    }
    files = set()
    with DSStore.open(filename,"r+") as d:
        for f in d:
            try:
                filename = f.filename
                logging.debug("name: '{}' code: '{}' type: '{}' value: '{}'".format(f.filename, f.code, f.type, f.value))
                if f.code in [b"BKGD", b"ICVO", b"fwi0", b"fwsw", b"fwvh", b"icsp", b"icvo", b"icvt", b"logS", b"lg1S", b"lssp", b"lsvo", b"lsvt", b"modD", b"moDD", b"phyS", b"ph1S", b"pict", b"vstl", b"LSVO", b"ICVO", b"dscl", b"icgo", b"vSrn"]:
                    logging.debug("Entry '{}' has code '{}' so assume directory.".format(filename, f.code))
                    entries["directories"].add(filename)
                else:
                    logging.debug("Entry '{}' has code '{}' so assume not directory.".format(filename, f.code))
                    files.add(filename)
            except Exception as e:
                logging.warning("Error parsing item: {}".format(e))
    logging.debug("Checking suspected files aren't in the directory list...")
    for file_ in files:
        if file_ not in entries["directories"]:
            entries["files"].add(file_)
    return entries

def print_version():
    """Print command line version banner."""
    print(""".       .1111...          | Title: DS_Store Parser {0}
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description: Extract filenames from .DS_Store
                    ..    | file.
                   ..     | Requires: ds_store
GrimHacker        ..      | This program comes with ABSOLUTELY NO WARRANTY;
                 ..       | for details type `show w'. This is free software,
grimhacker.com  ..        | and you are welcome to redistribute it under
@grimhacker    ..         | certain conditions.
----------------------------------------------------------------------------
""".format(__version__))

def output_json(output, data):
    class SetEncoder(json.JSONEncoder):
        # From https://stackoverflow.com/questions/8230315/how-to-json-serialize-sets
        def default(self, obj):
            if isinstance(obj, set):
                return list(obj)
            return json.JSONEncoder.default(self, obj)

    with open(output, "w") as f:
        logging.debug("Writing JSON to '{}'".format(output))
        json.dump(data, f, cls=SetEncoder, sort_keys=True, indent=4, separators=(',', ': '))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="Filepath of .DS_Store file", required=True)
    parser.add_argument("-V", "--version", help="Show version banner", action="store_true")
    parser.add_argument("-v", "--verbose", help="Debug logging", action="store_true")
    parser.add_argument("-o", "--output", help="JSON output filename")
    args = parser.parse_args()

    if args.version:
        print_version()
        exit()

    filename = args.input

    level = logging.INFO
    if args.verbose:
        level = logging.DEBUG
    logging.basicConfig(level=level, format="%(levelname)s: %(message)s")

    try:
        entries = parse_dstore_data(filename)
    except Exception as e:
        logging.critical("Error parsing file! {}".format(e))
    else:
        for type_ in entries:
            print("\n\nType: {} :".format(type_))
            print("\n".join(entries[type_]))

    if args.output:
        output_json(args.output, entries)

if __name__ == "__main__":
    main()
